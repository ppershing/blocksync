#!/usr/bin/env python
# vim: et
"""
Synchronise block devices over the network

Copyright 2006-2008 Justin Azoff <justin@bouncybouncy.net>
Copyright 2011 Robert Coup <robert@coup.net.nz>
Copyright 2012 Peter Peresini <ppershing+blocksync@gmail.com>

License: GPL

Getting started:

* Copy blocksync.py to the home directory on the remote host
* Make sure your remote user can either sudo or is root itself.
* Make sure your local user can ssh to the remote host
* Invoke:
    sudo python blocksync.py --direction push/pull /dev/local user@dsthost /dev/remote
"""
import abc
import sys
from sha import sha
import subprocess
import time
import os
import Queue as queue
import threading

KBYTE = 1024
MBYTE = 1024 * 1024

# Maximum size of IO queue for reading blocks from the disk
MAX_QUEUE_BYTES = 100 * MBYTE

BLOCKSIZE = 32768
BATCH_COUNT = 256 # by default 4M

PRINT_INTERVAL = 1

__VERSION__ = "blocksync v2.0"

def do_open(f, mode):
    f = open(f, mode)
    f.seek(0, 2)
    size = f.tell()
    f.seek(0)
    return f, size

### {{{ Block
class Block:
    def __init__(self, id, data):
        self.id = id
        self.data = data
        self.digest = sha(data)
### }}}

### {{{ Batch
class Batch:
    def __init__(self):
        self.blocks = []
        self.digest = sha()

    def add_block(self, block):
        self.blocks.append(block)
        self.digest.update(block.digest.hexdigest())
### }}}

### {{{ BlockReader
class BlockReader(threading.Thread):
    def __init__(self, f, blocksize, output_queue):
        threading.Thread.__init__(self)
        self.my_queue = output_queue
        self.f = f
        self.blocksize = blocksize

    def getblocks(self, f, blocksize):
        while 1:
            block = f.read(blocksize)
            if not block:
                break
            yield block

    def run(self):
        for i, block in enumerate(self.getblocks(self.f, self.blocksize)):
            self.my_queue.put((i, block), True)
        self.my_queue.put((-1, ""), True)
### }}}

### {{{ BlockHasher
class BlockHasher(threading.Thread):
    def __init__(self, input_queue, output_queue):
        threading.Thread.__init__(self)
        self.input_queue = input_queue
        self.output_queue = output_queue

    def run(self):
        finished = False
        while not finished:
            (i, block) = self.input_queue.get(True)
            self.output_queue.put(Block(i, block), True)
            self.input_queue.task_done()
            if (i == -1):
                finished = True
### }}}

### {{{ PipeWithStats
class PipeWithStats:
    def __init__(self, f):
        self.f = f
        self.read_bytes = 0
        self.write_bytes = 0

    def read(self, size):
        data = self.f.read(size)
        self.read_bytes += len(data)
        return data

    def write(self, data):
        self.write_bytes += len(data)
        return self.f.write(data)

    def flush(self):
        self.f.flush()

    def readline(self):
        data = self.f.readline()
        self.read_bytes += len(data)
        return data
### }}}

### {{{ BatchWorker
class BatchWorker:
    def run(self, f, blocksize, callback):
        read_queue = queue.Queue(MAX_QUEUE_BYTES / blocksize)
        hash_queue = queue.Queue(MAX_QUEUE_BYTES / blocksize)

        reader = BlockReader(f, blocksize, read_queue)
        hasher = BlockHasher(read_queue, hash_queue)

        reader.setDaemon(True)
        reader.start()
        hasher.setDaemon(True)
        hasher.start()

        finished = False
        while not finished:
            batch = Batch()

            for tmp in range(BATCH_COUNT):
                block = hash_queue.get(True)
                if (block.id == -1):
                    finished = True
                    break;
                batch.add_block(block)

            callback(batch)
### }}}

### {{{ Stats
class Stats:
    def __init__(self, logfile, p_in, p_out):
        self.logfile = logfile
        self.p_in = p_in
        self.p_out = p_out

        self.t_start = None
        self.t_last = None

        self.same_blocks = 0
        self.diff_blocks = 0
        self.done_blocks = 0

        self.same_batches = 0
        self.diff_batches = 0
        self.done_batches = 0

        self.blocksize = None
        self.size_blocks = None

    def have_block(self, is_same, count=1):
        self.done_blocks += count
        if is_same:
            self.same_blocks += count
        else:
            self.diff_blocks += count

    def have_batch(self, is_same):
        self.done_batches += 1
        if is_same:
            self.same_batches += 1
        else:
            self.diff_batches += 1

    def start(self):
        self.t_start = time.time()
        self.t_last = self.t_start

    def elapsed_time(self):
        return time.time() - self.t_start

    def print_stats(self):
        t1 = time.time()
        if t1 - self.t_last > PRINT_INTERVAL or self.done_blocks >= self.size_blocks:
            elapsed_time = self.elapsed_time() + 0.01 # avoid division by zero
            rate = (self.done_blocks + 1.0) * self.blocksize / MBYTE / elapsed_time
            t_eta = (self.size_blocks / (self.done_blocks + 1.0) - 1) * elapsed_time
            percent_done = 100.0 * self.done_blocks / (self.size_blocks + 0.01)

            str_time = "time: %ds" % elapsed_time
            str_synced = "synced: %.2f%%" % percent_done
            str_blocks = "blocks: (diff %d/done %d/total %d)" % (self.diff_blocks, self.done_blocks, self.size_blocks)
            str_batches = "batches: (diff %d/done %d/total ???)" % (self.diff_batches, self.done_batches)
            
            t_eta_s = t_eta % 60
            t_eta_m = (t_eta / 60) % 60
            t_eta_h = t_eta / 3600
            str_eta = "ETA: %d:%02d:%02d" % (t_eta_h, t_eta_m, t_eta_s)
            str_rate = "rate: %.1fMB/s" % rate
            str_net = "net: (up %.2f/down %.2f)" % \
                (float(self.p_out.write_bytes) / MBYTE, float(self.p_in.read_bytes) / MBYTE)
            str_diff = "diff: %.2fMB" % (self.diff_blocks * self.blocksize * 1.0 / MBYTE)

            if os.isatty(self.logfile.fileno()):
                print >>self.logfile, "\r\033[2A[ %s %s %s %s %s\033[K\n  %s %s %s]" % \
                    (str_time, str_synced, str_eta, str_rate, str_diff,
                     str_blocks, str_batches, str_net)
            else:
                print >>self.logfile, str_time, str_synced, str_blocks, str_batches, \
                    str_eta, str_rate, str_net, str_diff
            self.t_last = t1
### }}}

""" Sync protocol:
    Protocol works over blocks and batches of blocks.
    Batches help to save bandwidth in case there are big sequential parts of file without modifications
    
    Protocol:
    1.  pusher -> puller: "batch-digest $hexdigest"
    2a. puller -> pusher: "batch-skip"
        (batch digest same i.e. all blocks in the batch are same)
        continue with the next batch (step 1)
    2b. puller -> pusher: "batch-diff"
        we are going to see the diff inside the batch
    3.  for each block inside the batch
        puller -> pusher: "hash $blockno $hexdigest"
    4.  pusher determines which blocks are different, for each block
        a: pusher->puller: "same"
        b: pusher->puller: "diff", "$data"

    Note: Steps 3 and 4 operate over all blocks in the batch without waiting for the other side..
          This way we can save roundtrip latency, which improves throughput, especially for cases
          where only a few blocks out of a batch were different.
    """

class Pusher:
    def __init__(self, pipe_in, pipe_out, logfile, f_read, f_write, total_size, blocksize, nomodify=False):
        self.p_in = pipe_in
        self.p_out = pipe_out
        self.logfile=logfile
        self.f_read = f_read
        self.f_write = f_write
        self.size = total_size
        self.blocksize = blocksize
        self.nomodify = nomodify
        self.stats = Stats(logfile, pipe_in, pipe_out)

    def run(self):
        self.stats.start()
        self.stats.blocksize = self.blocksize
        self.stats.size_blocks = self.size / self.blocksize

        batcher = BatchWorker()
        batcher.run(self.f_read, self.blocksize,  self.process_batch)
        print >>self.logfile, "\nSynchronization completed in %d seconds" % self.stats.elapsed_time()
        return

    def process_batch(self, batch):
        self.p_out.write("batch-digest %s\n" % batch.digest.hexdigest())
        self.p_out.flush()

        res = self.p_in.readline().strip().split()
        assert res[0] in ["batch-skip", "batch-diff"]

        if res[0] == "batch-skip":
            self.stats.have_batch(is_same = True)
            self.stats.have_block(is_same = True, count = len(batch.blocks))
            self.stats.print_stats()
            return

        self.stats.have_batch(is_same = False)

        for block in batch.blocks:
            self.p_out.write("hash %d %s\n" % (block.id, block.digest.hexdigest()))
        self.p_out.flush()

        for block in batch.blocks:
            res = self.p_in.readline().strip().split()
            assert res[0] in ["same", "diff"]
            assert int(res[1]) == block.id

            if res[0] == "diff" and not self.nomodify:
                newblock = self.p_in.read(self.blocksize)
                pos = block.id * self.blocksize
                self.f_write.seek(pos)
                self.f_write.write(newblock)
            
            self.stats.have_block(res[0] == "same")
            self.stats.print_stats()
        return

class Puller:
    def __init__(self, pipe_in, pipe_out, logfile, f_read, total_size, blocksize, nomodify = False):
        self.p_in = pipe_in
        self.p_out = pipe_out
        self.logfile = logfile
        self.stats = Stats(logfile, pipe_in, pipe_out)
        self.f_read = f_read
        self.blocksize = blocksize
        self.size = total_size
        self.nomodify = nomodify
        
        self.stats.blocksize = self.blocksize
        self.stats.size_blocks = self.size / self.blocksize

    def run(self):
        print >>self.logfile, "Running puller"
        self.stats.start()

        batcher = BatchWorker()
        batcher.run(self.f_read, self.blocksize, self.process_batch)
        print >>self.logfile, "Synchronization completed in %d seconds" % self.stats.elapsed_time()
        return

    def process_batch(self, batch):
        receive = self.p_in.readline().strip().split()
        assert receive[0] == "batch-digest"
        if receive[1] == batch.digest.hexdigest():
            self.p_out.write("batch-skip\n");
            self.p_out.flush()
            self.stats.have_batch(is_same = True)
            self.stats.have_block(is_same = True, count = len(batch.blocks))
            self.stats.print_stats()
            return

        self.p_out.write("batch-diff\n");
        self.p_out.flush()
        self.stats.have_batch(is_same = False)

        for block in batch.blocks:
            receive = self.p_in.readline().strip().split()
            assert receive[0] == "hash"
            assert int(receive[1]) == block.id

            if receive[2] == block.digest.hexdigest():
                self.p_out.write("same %d\n" % block.id)
                self.stats.have_block(is_same = True)
            else:
                self.p_out.write("diff %d\n" % block.id)
                if not self.nomodify:
                    self.p_out.write(block.data)
                self.stats.have_block(is_same = False)

            self.stats.print_stats()
        self.p_out.flush()
        return

class Server:
    def run(self):
        p_in = PipeWithStats(sys.stdin)
        p_out = PipeWithStats(sys.stdout)
        logfile = open("blocksync.log", "w")
        p_out.write(__VERSION__ + "\n")
        p_out.flush()

        res = p_in.readline().strip().split()
        assert res[0] == "direction"
        assert res[1] in ["push", "pull", "nomodify"]
        direction = res[1]

        res = p_in.readline().strip().split()
        assert res[0] == "dev"
        dev = res[1]
        print >>logfile, "dev:", dev

        res = p_in.readline().strip().split()
        assert res[0] == "blocksize"
        blocksize = int(res[1])

        f_read, my_size = do_open(dev, 'r')
        p_out.write("size %d\n" % my_size)
        p_out.flush()

        p_out.write("can_sync\n")
        p_out.flush()

        if direction == "pull":
            puller = Puller(p_in, p_out, logfile, f_read, my_size, blocksize)
            puller.run()
        elif direction == "push":
            f_write, tmp_size = do_open(dev, 'r+')
            assert tmp_size == my_size
            pusher = Pusher(p_in, p_out, logfile, f_read, f_write, my_size, blocksize)
            pusher.run()
        elif direction == "nomodify":
            puller = Puller(p_in, p_out, logfile, f_read, my_size, blocksize, nomodify=True)
            puller.run()
        else:
            assert False
        return

class Client:
    def __init__(self, direction, srcdev, dsthost, dstdev, blocksize=BLOCKSIZE):
        self.srcdev = srcdev
        self.dsthost = dsthost
        self.dstdev = dstdev
        self.blocksize = blocksize
        self.direction = direction
        self.can_sync = False
        print "Block size is %0.2f KB" % (float(blocksize) / KBYTE)

    def connect(self):
        cmd = ['ssh', '-c', 'blowfish', self.dsthost, 'python', 'blocksync.py', '--server']
        print "Running: %s" % " ".join(cmd)

        p = subprocess.Popen(cmd, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, close_fds=True)
        p_out = PipeWithStats(p.stdin)
        p_in = PipeWithStats(p.stdout)

        line = p_in.readline()
        p.poll()
        if p.returncode is not None:
            print "Error connecting to or invoking blocksync on the remote host!"
            sys.exit(1)

        if line.strip() != __VERSION__:
            print "Wrong version of blocksync on the other side"
            sys.exit(1)

        p_out.write("direction %s\n" % self.direction)
        p_out.write("dev %s\n" % self.dstdev)
        p_out.write("blocksize %s\n" % self.blocksize)
        p_out.flush()

        try:
            f_read, my_size = do_open(self.srcdev, 'r')
        except Exception, e:
            print "Error accessing source device! %s" % e
            sys.exit(1)

        print "getting size"
        res = p_in.readline().strip().split()
        assert res[0] == "size"
        remote_size = int(res[1])
        assert my_size == remote_size

        print "Waiting for ready-to-go confirmation"
        # final confirmation that we can sync!
        res = p_in.readline().strip()
        assert res == "can_sync"

        self.size = my_size
        self.p_in = p_in
        self.p_out = p_out
        self.f_read = f_read
        self.can_sync = True
        self.logfile = sys.stdout

    def run(self):
        assert self.can_sync
        print "Synchronizing..."
        if self.direction == "push":
            puller = Puller(self.p_in, self.p_out, self.logfile,
                    self.f_read, self.size, self.blocksize)
            puller.run()
        elif self.direction == "pull":
            f_write, tmp_size = do_open(self.srcdev, 'r+')
            assert tmp_size == self.size
            pusher = Pusher(self.p_in, self.p_out, self.logfile,
                    self.f_read, f_write, self.size, self.blocksize)
            pusher.run()
        elif self.direction == "nomodify":
            pusher = Pusher(self.p_in, self.p_out, self.logfile,
                    self.f_read, None, self.size, self.blocksize, nomodify=True)
            pusher.run()

        else:
            assert False






if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser(usage="%prog [options] --direction push/pull /dev/source user@remotehost [/dev/dest]")
    parser.add_option("-b", "--blocksize", dest="blocksize", action="store", type="int",
                        help="block size (bytes)", default=BLOCKSIZE)
    parser.add_option("--direction", dest="direction", action="store", type="string",
                        help="(required) direction of sync: \
                            'push' - overwrite remote, \
                            'pull' - overwrite local, \
                            'nomodify' - just estimate size of the diff)", default=None)
    parser.add_option("--server", dest="server", action="store_true",
                        help="(used internally!)", default=False)
    (options, args) = parser.parse_args()



    if options.server:
        server = Server()
        server.run()
    else:
        if len(args) != 3:
            parser.print_help()
            print __doc__
            sys.exit(1)
        if options.direction not in ["push", "pull", "nomodify"]:
            parser.print_help()
            sys.exit(1)
        srcdev = args[0]
        dsthost = args[1]
        dstdev = args[2]

        client = Client(options.direction, srcdev, dsthost, dstdev, options.blocksize)
        client.connect()
        client.run()
